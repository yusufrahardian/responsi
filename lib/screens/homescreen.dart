import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:responsi2/page/item1.dart';
import 'package:responsi2/page/item2.dart';
import 'package:responsi2/page/item3.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Menu Wallpaper"),
        backgroundColor: Colors.black12,
      ),
      backgroundColor: Colors.white10,
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text(
                "Muhammad Yusuf Rahardian",
                style:
                    new TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
              ),
              accountEmail: new Text("Muhammad Yusuf Rahardian"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://i.pinimg.com/originals/62/3a/a8/623aa8f9933ee9a286871bf6e0782538.jpg"),
              ),
            ),
            ListTile(
                title: Text('Logout'),
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.logout),
                )),
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            MyMenu(
              title: "Vituber",
              icon: Icons.now_wallpaper,
              warna: Colors.lightBlue,
              menu: Item1(),
            ),
            MyMenu(
              title: "Anime",
              icon: Icons.image,
              warna: Colors.blue,
              menu: Item2(),
            ),
            MyMenu(
              title: "About",
              icon: Icons.info,
              warna: Colors.grey,
              menu: Item3(),
            )
          ],
        ),
      ),
    );
  }
}

class MyMenu extends StatelessWidget {
  MyMenu({this.title, this.icon, this.warna, this.menu});

  final String title;
  final IconData icon;
  final MaterialColor warna;
  final menu;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => menu));
        },
        splashColor: Colors.grey,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: 70.0,
                color: warna,
              ),
              Text(
                title,
                style: new TextStyle(fontSize: 17.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
