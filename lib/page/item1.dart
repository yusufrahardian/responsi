import 'package:flutter/material.dart';

void main() {
  runApp(Item1());
}

class Item1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(brightness: Brightness.dark),
        home: Scaffold(
          appBar: AppBar(
            title: Text(
              "Vtuber",
            ),
            leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back)),
          ),
          body: Listdata(),
        ));
  }
}

class Listdata extends StatefulWidget {
  const Listdata({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListdataState createState() => _ListdataState();
}

class _ListdataState extends State<Listdata> {
  List names = [
    "Dennou Shoujo Siro",
    "Cocoa Music",
    "Mirai Akari",
    "Kizuna Ai",
    "HimeHina",
    "Moemi",
    "Nekomiya Hinata",
    "Kaguya luna",
    "Tsukino Mito",
    "YuNi",
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          final number = index + 1;
          final name = names[index].toString();
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'images/_$number.jpg',
                    width: 140,
                    fit: BoxFit.cover,
                  ),
                  Text(name, style: TextStyle(fontSize: 18)),
                  Icon(
                    Icons.favorite,
                    color: Colors.pink,
                    size: 24.0,
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: names.length,
      ),
    );
  }
}
