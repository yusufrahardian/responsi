import 'package:flutter/material.dart';

void main() {
  runApp(Item2());
}

class Item2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(brightness: Brightness.dark),
        home: Scaffold(
          appBar: AppBar(
            title: Text(
              "Anime",
            ),
            leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back)),
          ),
          body: Listgambar(),
        ));
  }
}

class Listgambar extends StatefulWidget {
  const Listgambar({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListgambarState createState() => _ListgambarState();
}

class _ListgambarState extends State<Listgambar> {
  List names = [
    "Kizu musume",
    "Neko girl",
    "Hatsune miku",
    "Blach",
    "Swort Art Online",
    "One piece",
    "Sharinggan",
    "Swort Art Online S2",
    "Kaguya",
    "Emilia",
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          final number = index + 1;
          final name = names[index].toString();
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'gambar/_$number.jpg',
                    width: 140,
                    fit: BoxFit.cover,
                  ),
                  Text(name, style: TextStyle(fontSize: 18)),
                  Icon(
                    Icons.favorite,
                    color: Colors.pink,
                    size: 24.0,
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: names.length,
      ),
    );
  }
}
